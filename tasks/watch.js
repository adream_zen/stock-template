'use strict';

import gulp 		from 'gulp';
import watch		from 'gulp-watch';
import config		from './config'

import browserSync	from 'browser-sync';

export default () => {
	browserSync.create();
	browserSync.init({
		ui: {
		    port: config.ui_port
		},
		port: config.port,
        proxy: config.proxy,
        logLevel: 'debug'

    });
	gulp.watch( config.styles.source, 		['styles']);
	// gulp.watch( config.template_pattern,	['move']);
	gulp.watch( config.images.source, 		['images']);
	gulp.watch( config.scripts.allsource, 	['scripts']);
}