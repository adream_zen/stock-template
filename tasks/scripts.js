'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';
import browserSync	from 'browser-sync';

import babel		from 'gulp-babel';
import babelify		from 'babelify';
import browserify	from 'browserify';
import source		from 'vinyl-source-stream';
import buffer		from 'vinyl-buffer';
import uglify 		from 'gulp-uglify';

export default () => {
	let bundler = browserify( config.scripts.source );
	bundler.transform( "babelify", {
		presets: ['es2015']
	});
	bundler.transform( "babelify", {
		presets: ['es2015']
	} );
	
	return bundler.bundle()
		.on('error', function(error) {
			reportErr(error, this);
		})
		.pipe( source('scripts.js') )
		.pipe( buffer() )
		.pipe( uglify() )
		.pipe( gulp.dest( config.scripts.destination ) )
		.pipe( browserSync.stream() );
}

