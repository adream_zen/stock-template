import $    from 'jquery';


export default function( selector ){
	$(selector+'__contenido').hide();
	$(selector).on('click',selector+'__titulo',function() {
		var t = $(this);
		var p = t.parent().siblings().find(selector+'__contenido');
		var tp = t.next();
		var temp = 200;
		
		if ( t.hasClass('active')){
				t.removeClass('active');
		} else{
			$(selector+'__titulo').removeClass('active');
			 t.toggleClass('active');
		};
		p.slideUp(temp);
		tp.slideToggle(temp);
	});
}