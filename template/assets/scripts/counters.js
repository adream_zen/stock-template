import $ 	from 'jquery';

export default function( selector ) {
	if(selector.length != undefined){
		$(selector).each(function () {
			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 4000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	}
}