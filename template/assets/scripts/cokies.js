import $    from 'jquery';

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var koniec = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + koniec + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var akcja = getCookie("ciasteczko");
  if (akcja != "") {
   $("#pasek").css("bottom", "-60px");
  } else {
    $("#pasek").css("bottom", "0");
    $("#przycisk").click(function() {
      akcja = "1";
      setCookie("ciasteczko", akcja, 30);
      $( "#pasek" ).animate({ bottom: "-60px" }, 500 );
    });
  }
}
export default function() {
  checkCookie();
  $("#przycisk").click(function() {
    checkCookie();
  });
};