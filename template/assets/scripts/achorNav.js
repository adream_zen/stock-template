import $	from 'jquery';

export default function() {
	$(document).on('click', 'a[href*=\\#]', function(event){
		$("input#m_nav").prop('checked', false);
		var href = $(this).attr("href");
		href = href.split("#").pop();
		$('html, body').animate({
			scrollTop: $("[name="+href+"]").offset().top
		},2000);
	});
}