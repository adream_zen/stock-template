import $ 	from 'jquery';

export default function( selector="" ) {
	
	if(selector.length){
		var selector = selector;
	
		selector.hover(function(){
			selector.on("mousemove", function( event ){
				var parentOffset = $(this).offset();
				var posX = (event.pageX - parentOffset.left) / $(this).width() * 100;
				var posY = (event.pageY - parentOffset.top) / $(this).height() * 100;
				$(this).css("background-position", posX+"% "+posY+"%");
			})
		})
		selector.mouseleave(function(){
				$(this).css("background-position", "50% 50%");
		})
	}
}