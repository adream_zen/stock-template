import $	from 'jquery';

export default function() {
	$('.first-word').each(function(){
		var me = $(this);
		me.html( me.html().replace(/^(\w+)/, '<span>$1</span>') );
	});
	$('.last-2-word').each(function(){
		var me = $(this);
		me.html( me.html().replace(/(\S+\s+\S+?)$/, '<span>$1</span>') );
	});
	$('.last-word').html(function() {
		var me = $(this);
		me.html( me.html().replace(/(\w+)$/, '<span>$1</span>') );
	})
}