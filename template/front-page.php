<?php get_header(); ?>
	<main class="parent parent--content">
		<section class="row container">
			
		<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					echo '<h1>';
					the_post();
					echo '</h1>';
					the_title();
					the_content();
				} // end while
			} // end if
		?>
		</section>
	</main>
<?php get_footer(); ?>