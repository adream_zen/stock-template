<?php
/**
 *
 * @package Adream
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="icon" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon.png">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/preloader.css" type="text/css" media="all">
	<?php wp_head(); ?>
	<script type="text/javascript">
		var Preloader = function() {
			document.getElementById("loader").removeAttribute("class");
			document.getElementById("loader").style.opacity = "0";
		};
		window.onload = Preloader;
	</script>
</head>

<body <?php body_class(); ?>>
	<div id="loader" class="loader"></div>
<!--[if lt IE 10]>
	<div class="alert alert-warning">
		You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
	</div>
<![endif]-->
	<?php // get_template_part('view/parts/image-extender'); ?>
	<?php // get_template_part('view/parts/cookies'); ?>
	<!--<div id="psd"></div>
	<div id="psd_switch">PSD ON/OFF</div> -->
	<!-- <div id="go_to_top"></div> -->
<div class="wrapper">
	<header class="parent parent--header">
		<section class="row container">
			<?php  get_template_part('view/parts/nav'); ?>
		</section>
	</header>