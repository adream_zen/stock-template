<?php
/**
 *
 * @package Adream
 */

?>
	<footer class="parent parent--footer">
		<section class="row container row--footer">
			<div class="col col--3">
				<?php img("logo.png","logo") ?>
				<p><?php bloginfo('description'); ?> </p>
				<p>ul. dolor 54, 25-587 Lorem</p>
				<p><a href="tel:122961623" class="href">12 296 16 23</a></p>
				<p><a href="mailto:" class="href">biuro@email.com</a></p>
			</div>
			<div class="col col--3">
				<h5>Strony</h5>
				<?php wp_nav_menu() ?>
			</div>
			<div class="col col--3">
				<h5>Kontakt</h5>
				<div class="col__row">
					<a href="tel:122961623" class="href href--tel">Masz Pytanie?<br>12 296 16 23</a>
				</div>
				<div class="col__row">
					<a href="mailto:" class="href href--mail">Wyślij wiadomość</a>
				</div>
			</div>
		</section>
	</footer>
	<div class="parent parent--copyright" >
		<section class="row container row--copyright">
			<div class="col col--2">&copy;<?php echo date("Y"); echo ' | '; bloginfo('title'); ?></div>
			<div class="col col--2">
				<span>Projekt i realizacja</span> <a href="zensite.pl"><?php img("zensite.png","zensite") ?></a>
			</div>
		</section>
	</div>
</div> 
<?php wp_footer(); ?>
<link rel="stylesheet" id="adream-scss-css" href="<?php echo get_template_directory_uri(); ?>/assets/styles/style.css" type="text/css" media="all">
</body>
</html>