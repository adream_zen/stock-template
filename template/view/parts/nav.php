<input type="checkbox" id="m_nav">
<div class="header--wrap">
	<div class="logo fixed">
		<a href="<?php echo get_home_url() ?>">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="logo">
		</a>
	</div>
	<label for="m_nav" id="mobile-hamburger" onclick>
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</label>
</div>
<nav>
	<?php wp_nav_menu() ?>
</nav>