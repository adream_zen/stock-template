<?php get_header(); ?>
	<main class="parent parent--404">
		<div class="row container">
			<div class="col col--1">
				<h1>404</h1>
				<h2>Niestety strona na która próbuje się dostać jest niedostepna lub nie istnieje</h2>
				<h3>Przepraszamy</h3>
				<a href="<?php echo get_home_url() ?>" class="button button--404 button--center">
					Potrów do strony głównej
				</a>
			</div>
		</div>
	</main>
<?php get_footer(); ?>