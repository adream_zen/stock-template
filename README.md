# Wordpress Stock Template

### Gulp tasks
- Clean - Clear dest directory.
- [Images](https://github.com/1000ch/gulp-image) - Compress images and save in dest direcory. [Require IM](https://www.imagemagick.org/download/binaries/ImageMagick-7.0.6-10-Q16-x64-dll.exe) and [Require GM](http://www.graphicsmagick.org)
- Scripts - Import, convert, compress scripts and save in dest directory.
- Styles - Import and compress sass styles and save in dest directory.
- Move - Move other files to dest folder
- [Watch](https://github.com/floatdrop/gulp-watch) - Check files changes and reload browser by BrowseSync.
- [Sql](https://github.com/webcaetano/mysqldump) - Data base backup.

#All template changes do in template directory.

### Suggest to use technologies for style
- [BEM](https://nafrontendzie.pl/metodyki-css-2-bem/)
- [SASS/SCSS](http://www.merixstudio.pl/blog/co-daje-sass-dlaczego-warto-uzywac-sass/)

### #1 Geting start
After install wordpress

By comand line get into wp-content/themes and make dir
```bash
	mkdir adream_theme && cd adream_theme
```

Clone repository
```bash
	git clone git@bitbucket.org:adream_zen/stock-template.git _project
```
Get into _project folder
```bash
	cd _project
```
Change git repository
```bash
    git config remote.origin.url git@bitbucket.org:Adream_zen/nowe.git
```
Check git config
```bash
    git config --list
```
Install node.js with modules
```bash
	npm install
```
Change gulp config file "config.json"
```bash	
	"proxy": "localhost/stock", <- to localhost/[your project name]
	"sql":{
		"host": 		"localhost", 	<- to [your database host]
		"user": 		"root",			<- to [your database user]
		"password": 	"",				<- to [your database pass]
		"database": 	"syncret_db" 	<- to [your database name]
	}
```

For use devlopment gulp, type comand:
```bash
	gulp run
```

For create server fianl file version, use:
```bash
	gulp final
```

Backup database
```bash
	gulp sql
```

* * *

###### Include
- [node.js](https://nodejs.org/en/)
- [gulp.js](http://gulpjs.com) + tasks
- [ECMAScript 6](https://babeljs.io/learn-es2015/) [PL](http://mateuszroth.pl/komp/article-javascriptes6.php)
- Babel
- [SCSS](http://sass-lang.com)
	- [Normalize.scss](http://nicolasgallagher.com/about-normalize-css/)
	- [Bootstrap v4 - alpha](https://github.com/twbs/bootstrap/tree/v4-dev)
	- HTML vs. PSD
	- [Responsive navigation](https://codepen.io/Krzywy14/pen/WpMwXN)
- Plugins
	- tab swither
	- scroll to top
	- achor scroll
	- parallax
	- user scroll stop
	- cokies plugin
	- wordSizer
	- [counters](https://codepen.io/shivasurya/pen/FatiB)
	- Adordion
	- Gallery based on img alt
	- Preloader
	- [Curtain](https://codepen.io/rontav/pen/rVbXqP)
- IE 10 if
- Random string length genrator(Lorem Ipsum)
- Wordpress fucntion:
	- custom content size
	- img include 
	- background image
	- Custom login page
	- Remove dashboard menu item


* * *

#Plugin exemples used

## Tabs
###### html
```bash
	<ul class="tabs__nav">
		<li>11111</li>
		<li>22222</li>
		<li>33333</li>
	</ul>
	<ul class="tabs__wrap">
		<li>111111</li>
		<li>222222</li>
		<li>333333</li>
	</ul>
```
###### Script
```bash
	
	import tabs from './tabs';

	$(()=>{
		new tabs(".tabs__nav", ".tabs__wrap");
	});

```
###### SCSS
```bash

	@import "components/tabs";

```

## Scroll to top

###### HTML - after <body>
```bash
	<div id="go_to_top"></div>
```
###### SCSS
```bash
	@import "components/scrollTop";
```
###### JavaScript
```bash
	import scrollTop 	from './scrollTop';
	
	$(()=>{
		new scrollTop();
	});
```

## User scroll - stop plugin scroll when user scoll
###### JavaScript
```bash
	import userScroll 	from './userScroll';

	$(()=>{
		new userScroll();
	});
```


## AchorNav - scroll to achor when link start with #

###### HTML
```bash
	<a href="#name">Achor name</a>


	<a name="name"></a>
```
###### JavaScript
```bash
	import achorNav 		from './achorNav';

	$(()=>{
		new achorNav();
	});
```


## Parallax
###### HTML
```bash
	<div id="parallax"></div>
```
###### SCSS
```bash
	background-image: url(img.jpg)
	background-position: center 40%;
```
###### JavaScript
```bash
	import parallax 		from './parallax';

	$(()=>{
		new parallax("#parallax");
	});
```



## Cookies
###### HTML
```bash
	<div class="cookie-bar" id="pasek">
		Ta strona wykorzystuje pliki Cookies do poprawnego działania.
		<a href="#0" class="accept-btn" id="przycisk">Akceptuj</a>
		<a href="<?php get_template_directory_uri();?>/polityka-cookies" class="more-btn">Więcej</a>
	</div>
```
###### SCSS
```bash
	@import "components/cokies";
	
	change in this file:

	.accept-btn {
		/*  Stylujemy pod projekt  */
		background: $base;
		padding: 10px 20px;
		color: $base-color;
	}
```
###### JavaScript
```bash
	import cokies 		from './cokies';

	$(()=>{
		new cokies();
	});
```
## Parallax
###### HTML
```bash
	<div class="count">200</div>
```
###### JavaScript
```bash
	import counters 	from './counters';

	$(()=>{
		new tabs(".count");
	});
```

## Acordeon
###### HTML
```bash
	<ul class="acordeon">
		<li title="tytul 1">
			<p>Lorem ipsum dolor sit amet.</p>
		</li>
	</ul>
```
###### SCSS
```bash
	@import "components/acordeon";
```
###### JavaScript
```bash
	import acordeon 	from './acordeon';

	$(()=>{
		new acordeon(".acordeon",);
	});

```

## Gallery
###### HTML
```bash
	<img class="gallery__item" src="photo.jpg" alt="photo.jpg">
```
###### SCSS
```bash
	@import "components/gallery";

```
###### JavaScript
```bash
	import gallery			from './gallery';

	$(()=>{
		new gallery(".gallery__item");
	});
```
## Random text generator
###### HTML / PHP 
	Print random lenght string from lorem ipsum 50 words
```bash
	<?php random_text() ?> <!-- print 50 - 399 char -->
	<?php random_text(60) ?>  <!-- print 50 - 60 char -->
```
## Curtain
###### HTML
```bash
	<section id="curtain" class="section__slider curtain">
			<div class="hero hero--1">
			</div>	
	 </section>
	<div class="content">
	 	// rest page content
	</div>
```
###### SCSS
```bash
	@import "components/curtain";
```
###### JavaScript
```bash
	import curtain			from './curtain';

	$(()=>{
		new curtain();
	});
```

# Wordpress fuctions used
## Image include
###### HTML/PHP
```bash
	<?php img('nine.png'); ?>
```

## Custom conetent size
###### HTML/PHP
```bash
	<?php custom_content(50) ?>
```

## Background image
```bash
	<div <?php bgimg("image.jpg")) ?> ></div>
```
## Background image from thumbnail
```bash
	<div <?php bgth(get_the_post_thumbnail_url())) ?> ></div>
```
# add to wp-config.php to remove editor in dashboard
```bash
	define('DISALLOW_FILE_EDIT', true);
```